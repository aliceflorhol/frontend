import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: './home/home.module#HomePageModule'
  },
  {
    path: 'list',
    loadChildren: './list/list.module#ListPageModule'
  },
  { path: 'n-articulo', loadChildren: './n-articulo/n-articulo.module#NArticuloPageModule' },
  { path: 'fotos', loadChildren: './fotos/fotos.module#FotosPageModule' },
  { path: 'asignarcolor', loadChildren: './asignarcolor/asignarcolor.module#AsignarcolorPageModule' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
