import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsignarcolorPage } from './asignarcolor.page';

describe('AsignarcolorPage', () => {
  let component: AsignarcolorPage;
  let fixture: ComponentFixture<AsignarcolorPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsignarcolorPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsignarcolorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
