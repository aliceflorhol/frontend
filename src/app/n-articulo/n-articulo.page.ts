import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-n-articulo',
  templateUrl: './n-articulo.page.html',
  styleUrls: ['./n-articulo.page.scss'],
})
export class NArticuloPage implements OnInit {
  ocultarn: boolean = false;
  ocultarbtn1: boolean = false;
  ocultarbtnn: boolean = false;
  constructor() {
  }
  activar1(){
    this.ocultarn =!this.ocultarn;
    this.ocultarbtn1 = !this.ocultarbtn1;
  }
  activarn(){
    this.ocultarbtnn = !this.ocultarbtnn;
  }

  public onKeyUp(event: any) {
  let newValue = event.target.value;
  let regExp = new RegExp('^[A-Z0-9]+$');
  if (! regExp.test(newValue)) {
    event.target.value = newValue.slice(0, -1);
  }
}

  ngOnInit() {
  }

}
