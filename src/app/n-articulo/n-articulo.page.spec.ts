import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NArticuloPage } from './n-articulo.page';

describe('NArticuloPage', () => {
  let component: NArticuloPage;
  let fixture: ComponentFixture<NArticuloPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NArticuloPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NArticuloPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
